import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class LibroControlador {

    @FXML
    private TextField txtTitle;

    @FXML
    private TextField txtAutor;

    @FXML
    private TextField txtEditorial;

    @FXML
    private TextField txtYearEdition;

    @FXML
    private TextField txtPrice;

    @FXML
    private TextField txtPages;

    @FXML
    private TextField txtCopies;

    @FXML
    private Label txtCopiesAvailable;

    @FXML
    private Label txtBorrowedCopies;

    @FXML
    private Button cmdSaveChanges;

    @FXML
    private Button cmdUpdateWindow;

    @FXML
    private Button cmdRegisterLoan;

    @FXML
    private Button cmdRegisterReturn;

    @FXML
    void registerLoan() {
        if(libro.prestar()){
            txtCopiesAvailable.setText(libro.getNumeroDeCopiasDisponibles()+"");
            txtBorrowedCopies.setText(libro.getNumeroDeCopiasPrestadas()+"");
        } else {
            txtCopiesAvailable.setText("Pestamo no valido");           
        }
    }

    @FXML
    void registerReturn() {
        if(libro.devolver()){
            txtCopiesAvailable.setText(libro.getNumeroDeCopiasDisponibles()+"");
            txtBorrowedCopies.setText(libro.getNumeroDeCopiasPrestadas()+"");
        } else {
            txtBorrowedCopies.setText("Intento de devolucion no valido");
            System.out.println("No valido");            
        }
    }

    @FXML
    void saveChanges() {
        try {
            libro.setTitulo(txtTitle.getText());
            libro.setEditorial(txtEditorial.getText());
            libro.setAutores(txtAutor.getText());
            libro.setAñoEdicion(Integer.parseInt(txtYearEdition.getText()));
            libro.setNumeroDePaginas(Integer.parseInt(txtPages.getText()));
            libro.setPrecio(Integer.parseInt(txtPrice.getText()));
            boolean re = libro.cambiarNumeroDeCopias(Integer.parseInt(txtCopies.getText()));
            if(re) {
                txtCopiesAvailable.setText(libro.getNumeroDeCopiasDisponibles()+"");
                txtBorrowedCopies.setText(libro.getNumeroDeCopiasPrestadas()+"");
            }
        }
        catch(Exception e) {
          //  Block of code to handle errors
          txtBorrowedCopies.setText("Registro no valido, verifica los campos");
        }
        
    }

    @FXML
    void updateWindow() {

    }
    
    
    private Libro libro;
    
    public LibroControlador() {
        libro = new Libro();
    }

}
