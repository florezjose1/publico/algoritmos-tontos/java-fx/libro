### El Libro

* Interface en Java FX

![Img1](preview.png)


* Diagrama

![Img1](Diagrama.png)


Se requiere un programa para controlar el préstamo de un Libro. Es un único Libro con varias copias.

* Los datos básicos del libro son título, autores, editorial, año de edición, precio, cantidad de * páginas y cantidad de copias.
* Se debe permitir el registro de préstamos.
* Se debe permitir el registro de devoluciones.
* Debe poderse consultar la cantidad de copias disponibles.
* Al cambiar la cantidad de copias debe evitarse que se generen inconsistencias: la nueva cantidad de copias no puede ser inferior a  la cantidad de copias prestadas (¿por qué?).


Versión Bluej: `4.2.2`

Versión Java: `11.0.6`

Hecho con ♥ por [Jose Florez](https://joseflorez.co)

